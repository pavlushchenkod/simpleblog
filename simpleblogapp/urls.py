from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.home_page, name='home_page'),
    url(r'^(?P<article_id>[0-9]+)/$', views.show_article, name='show_article'),
    url(r'^add_like/(?P<article_id>[0-9]+)/$', views.add_like, name='add_like'),
    url(r'^registration$', views.registration, name='registration'),
    url(r'^activate/(?P<user_idb64>.*)$', views.activate, name='activate'),
    url(r'^login$', views.user_login, name='user_login'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
]
