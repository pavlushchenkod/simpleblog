from django.apps import AppConfig


class SimpleblogappConfig(AppConfig):
    name = 'simpleblogapp'
