from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage, InvalidPage
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.db.models import F
from django.contrib.auth import login, authenticate, logout
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.auth.models import User, AnonymousUser

from simpleblogapp.models import Article, Comments, Likes, Tags, UserInformation
from simpleblogapp.forms import RegistrationForm, CommentForm
from simpleblogapp.utils import pages_for_pagination


def home_page(request):
    if isinstance(request.user, AnonymousUser):
        return redirect('user_login')
    if request.user:
        if not request.user.is_authenticated:
            return redirect('user_login')

    if request.method == 'GET':
        articles_list = Article.objects.order_by('-pub_date')

    if request.method == 'POST':
        tag = request.POST['tag']
        articles_list = Article.objects.filter(tags__name=tag).order_by('-pub_date')

    paginator = Paginator(articles_list, 4)
    page = request.GET.get('page')

    try:
        articles = paginator.page(page)
    except (PageNotAnInteger, EmptyPage, InvalidPage):
        articles = paginator.page(1)
        page = 1

    list_of_pages = pages_for_pagination(int(page), paginator.num_pages)
    user_name = request.user.get_username()

    tag_list = Tags.objects.all().distinct()
    context = {'articles': articles,
               'list_of_pages': list_of_pages,
               'user_name': user_name,
               'tag_list': tag_list}
    return render(request, 'simpleblogapp/main.html', context)


def add_like(request, article_id):
    if isinstance(request.user, AnonymousUser):
        return redirect('user_login')
    if request.user:
        if not request.user.is_authenticated:
            return redirect('user_login')

        user_id = request.user.id
        # перевірка наявності лайка від юзера
        is_like = Likes.objects.filter(article_id=article_id,
                                       user_id=user_id).exists()
        # додаємо або видаляємо лайк
        if not is_like:
            new_like = Likes(
                article_id=article_id,
                user_id=user_id)
            new_like.save()
        else:
            Likes.objects.filter(article_id=article_id,
                                 user_id=user_id).delete()
        # рахуємо к-ть лайків для статті
        quantity_likes = Likes.objects.filter(article_id=article_id).count()
        # оновлюємо к-ть лайків для статті
        Article.objects.filter(id=article_id).update(quantity_likes=quantity_likes)
        # повернення на поточну сторінку
        return HttpResponseRedirect(request.GET['next'])


def show_article(request, article_id):
    if isinstance(request.user, AnonymousUser):
        return redirect('user_login')
    if request.user:
        if not request.user.is_authenticated:
            return redirect('user_login')

        if request.method == 'GET':
            try:
                comments = Comments.objects.filter(article_id=article_id)
                article = Article.objects.get(id=article_id)
            except ObjectDoesNotExist:
                return redirect('/simpleblogapp/')

            paginator = Paginator(comments, 2)
            page = request.GET.get('page')

            try:
                comments = paginator.page(page)
            except (PageNotAnInteger, EmptyPage, InvalidPage):
                comments = paginator.page(1)

            user_name = request.user.get_username()

            context = {'article': article,
                       'article_id': article_id,
                       'comments': comments,
                       'user_name': user_name}
            return render(request, 'simpleblogapp/article.html', context)
        # додавання коментаря
        if request.method == 'POST':
            form = CommentForm(request.POST)

            if form.is_valid():
                user_id = request.user.id
                comment = Comments(
                    text=form.cleaned_data['comment'],
                    article_id=article_id,
                    user_id=user_id)
                comment.save()
                # оновлення кількості коментарів для статті
                Article.objects.filter(id=article_id).update(
                    quantity_comments=F('quantity_comments') + 1
                )
            return HttpResponseRedirect(request.GET['next'])


def registration(request):
    if request.method == 'GET':
        return render(request, 'simpleblogapp/registration.html')

    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            try:
                user = User.objects.create_user(
                    username=form.cleaned_data['name'],
                    email=form.cleaned_data['email'],
                    password=form.cleaned_data['password'],
                    is_active=False)
                user.save()

                user_id = user.id
                user_information = UserInformation(
                    user_id=user_id,
                    first_name=form.cleaned_data['first_name'],
                    last_name=form.cleaned_data['last_name'],
                    country=form.cleaned_data['country'],
                    city=form.cleaned_data['city'],
                    birthday=form.cleaned_data['birthday'])
                user_information.save()

            except IntegrityError:
                error = 'Email or name already used!'
                context = {'message': error}
                return render(request, 'simpleblogapp/registration.html', context)
            # надсилання повідомлення для підтвердження email
            user_idb64 = user.id
            current_domain = get_current_site(request).domain
            mail_message = render_to_string('simpleblogapp/active_email.html',
                                            context={'user_idb64': user_idb64,
                                                     'domain': current_domain})
            mail_subject = 'Thanks for signing up!'
            send_mail(mail_subject,
                      mail_message,
                      'simpleblogpdv@gmail.com',
                      [form.cleaned_data['email']],
                      fail_silently=True)

            context = {'message': 'Thank you for registering! '
                                  'We sent a link for verification to your email!'}
            return render(request, 'simpleblogapp/login.html', context)
        context = {'message': 'You entered incorrect data! Try again!'}
        return render(request, 'simpleblogapp/registration.html', context)


def activate(request, user_idb64):
    try:
        user_id = user_idb64
        user = User.objects.get(id=user_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None:
        user.is_active = True
        user.save()

        context = {'message': 'Thank you for your email confirmation. '
                              'Now you can login your account!'}
        return render(request, 'simpleblogapp/login.html', context)
    else:
        context = {'message': 'Activation link is invalid!'}
        return render(request, 'simpleblogapp/registration.html', context)


def user_login(request):
    if request.method == 'GET':
        context = {'message': 'You can enter for review! user:demo/password: demodemo'}
        return render(request, 'simpleblogapp/login.html', context)

    if request.method == 'POST':
        if not request.user.is_authenticated:
            data = request.POST
            user = authenticate(username=data['name'],
                                # email=data['email'],
                                password=data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home_page')

                context = {'message': 'Account not activated!'}
                return render(request, 'simpleblogapp/registration.html', context)
            context = {'message': 'You entered an incorrect login or password!'}
            return render(request, 'simpleblogapp/login.html', context)
        return redirect('user_login')


def user_logout(request):
    logout(request)
    return redirect('home_page')
